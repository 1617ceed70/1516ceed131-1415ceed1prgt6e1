/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;
import modelo.Grupo;

/**
 *
 * @author paco
 */
public class VistaGrupo implements IVista<Grupo> {

    @Override
    public void mostrar(HashSet hs) {
        Vista v = new Vista();
        Iterator it = hs.iterator();
        Grupo grupo = null;

        System.out.println("\nMOSTRAR GRUPOS");
        while (it.hasNext()) {
            grupo = (Grupo) it.next();
            System.out.println(grupo);
        }
    }

    @Override
    public Grupo obtener() {
        String nombre = null;
        String idalumno = null;
        Grupo grupo = new Grupo();

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        System.out.println("\nOBTENER GRUPO");

        System.out.print("Nombre: ");
        try {
            nombre = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.print("Id Alumno: ");
        try {
            idalumno = br.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Vista.class.getName()).log(Level.SEVERE, null, ex);
        }

        grupo.setNombre(nombre);
        return grupo;

    }

}
