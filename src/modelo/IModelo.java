/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public interface IModelo {

    // Alumno
    public void create(Alumno a); // Crea uno nuevo

    public void update(Alumno a); // Actuzaliza uno

    public void delete(Alumno a);  // Borrar uno

    public HashSet<Alumno> reada(); // Obtiene todos

    // Grupo
    public void create(Grupo g); // Crea uno nuevo

    public void update(Grupo g); // Actuzaliza uno

    public void delete(Grupo g);  // Borrar uno

    public HashSet<Grupo> readg(); // Obtiene todos

}
