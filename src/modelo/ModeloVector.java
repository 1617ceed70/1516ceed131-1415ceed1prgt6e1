package modelo;

import java.util.HashSet;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloVector implements IModelo {

    Alumno alumnos[] = new Alumno[100];
    Grupo grupos[] = new Grupo[100];

    Alumno vacioa = new Alumno();
    Grupo vaciog = new Grupo();

    int ida = 0;
    int idg = 0;

    public ModeloVector() {
        vacioa.setId(" ");
        vaciog.setId(" ");
        for (int i = 0; i < alumnos.length; i++) {
            alumnos[i] = vacioa;
        }
    }

    @Override
    public void create(Alumno alumno) {
        alumno.setId("" + ida);
        alumnos[ida] = alumno;
        ida++;

    }

    public void update(Alumno alumno) {

        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = alumno;
                return;
            }
            i++;
        }

    }

    @Override
    public void delete(Alumno alumno) {
        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = vacioa;
                return;
            }
            i++;
        }
    }

    @Override
    public void create(Grupo grupo) {
        grupo.setId("" + idg);
        grupos[idg] = grupo;
        idg++;
    }

    @Override
    public void update(Grupo grupo) {

        int i = 0;

        while (i < grupos.length) {
            if (grupos[i].getId().equals(grupo.getId())) {
                grupos[i] = grupo;
                return;
            }
            i++;
        }

    }

    @Override
    public void delete(Grupo grupo) {

        int i = 0;
        while (i < grupos.length) {
            if (grupos[i].getId().equals(grupo.getId())) {
                grupos[i] = vaciog;
                return;
            }
            i++;
        }
    }

    @Override
    public HashSet<Alumno> reada() {
        HashSet hs = new HashSet();
        int i = 0;
        for (i = 0; i < ida; i++) {
            if (!alumnos[i].getId().equals(" ")) {
                hs.add(alumnos[i]);
            }
        }
        return hs;
    }

    @Override
    public HashSet<Grupo> readg() {
        HashSet hs = new HashSet();
        int i = 0;
        for (i = 0; i < idg; i++) {
            if (!grupos[i].getId().equals(" ")) {
                hs.add(grupos[i]);
            }
        }
        return hs;
    }

}
