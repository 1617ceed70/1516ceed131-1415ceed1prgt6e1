/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloHashSet implements IModelo {

   HashSet alumnos = new HashSet();
   HashSet grupos = new HashSet();

   int ida = 0;
   int idg = 0;

   @Override
   public void create(Alumno alumno) {
      alumno.setId("" + ida);
      alumnos.add(alumno);
      ida++;
   }

   public void update(Alumno alumno) {

      Iterator it = alumnos.iterator();
      Alumno a;
      int pos = 0;

      while (it.hasNext()) {
         a = (Alumno) it.next();
         if (a.getId().equals(alumno.getId())) {
            alumnos.remove(a);
            // alumnos.add(alumno);
            // No se puede borrar y añadir en el mismo iterador
         }
      }
      alumnos.add(alumno);

   }

   public void delete(Alumno alumno) {

      Iterator it = alumnos.iterator();
      Alumno a;
      int pos = 0;

      while (it.hasNext()) {
         a = (Alumno) it.next();
         if (a.getId().equals(alumno.getId())) {
            alumnos.remove(a);
            break;
         }
      }

   }

   @Override
   public void create(Grupo g) {
      g.setId("" + idg);
      grupos.add(g);
      idg++;
   }

   @Override
   public void update(Grupo grupo) {
      Iterator it = grupos.iterator();
      Grupo g;
      while (it.hasNext()) {
         g = (Grupo) it.next();
         if (g.getId().equals(grupo.getId())) {
            grupos.remove(g);
            break;
         }
      }
      grupos.add(grupo);
   }

   @Override
   public void delete(Grupo grupo) {
      Iterator it = grupos.iterator();
      Grupo g;
      int pos = 0;

      while (it.hasNext()) {
         g = (Grupo) it.next();
         if (g.getId().equals(grupo.getId())) {
            grupos.remove(g);
            break;
         }
      }
   }

   @Override
   public HashSet<Grupo> readg() {
      return grupos;
   }

   @Override
   public HashSet<Alumno> reada() {
      return alumnos;
   }

}
